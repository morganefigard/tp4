$(
	function() {
		var total = 0;
		$('div .pizza-type > label').hover(
			function() {
		        $(this).find('.description').removeClass('no-display');
			}, function() {
				$(this).find('.description').addClass('no-display');
			}
		);
		$('div .nb-parts > input').change(
			function() {
				$('.pizza-pict').removeClass().addClass('pizza-' + $(this).val() + ' pizza-pict');
			}
		)
		$('.next-step').click(
			function() {
				$('.infos-client').removeClass('no-display');
				$(this).addClass('no-display');
			}
		)
		$('.add').click(
			function() {
				var input = $(this).prev().clone();
				$(this).before('<br />');
				$(this).before(input);
			}
		)
		$('.done').click(
			function() {
				$('div').remove();
				$('body').append('<p style="text-align: center;">Merci PRENOM ! Votre commande sera livrée dans 15 minutes.</p>');
			}
		)
		$('input[name=type], input[name=pate], input[name=extra]').change(
			function() {
				total = 0;
				$('input:checked').each(
					function() {
						total += $(this).data('price'); 
						$('div .tile > p').html(total + ' €');
					}
				);
			}
		)
	}
)
